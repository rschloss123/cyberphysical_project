import csv
import numpy as np
import matplotlib.pyplot as plt

f = open('/home/rachel/cyberphysical_project/states_and_vals.csv', 'rt')
reader = csv.reader(f)
row_count = sum(1 for row in reader)
f.seek(0)

col_count = len(next(reader))
# print "columns"
# print col_count

f.seek(0)
data = [row for row in reader] # list comprehension 
f.close()
string_col = data[0]
robot_state = []
battery = []
robot_complete = []
robot_delivery = []
work_load = []
human_complete = []
time_step = []
prioritize_charge =[]


for col in range(0, col_count):
	if (string_col[col] == 'robot_state'):
		robot_state_col = col 
	if (string_col[col] == 'battery'):
		battery_level_col = col
	if (string_col[col] == 'robot_complete'):
		robot_complete_col = col
	if (string_col[col] == 'robot_delivery'):
		robot_delivery_col = col
	if (string_col[col] == 'work_load'):
		work_load_col = col
	if (string_col[col] == 'human_complete'):
		human_complete_col = col
	if (string_col[col] == 'prioritize_charge'):
		prioritize_charge_col = col		


t = 0

for row1 in range(1,row_count):
	this_row = data[row1]
	robot_state.extend([int(this_row[robot_state_col])])
	battery.extend([int(this_row[battery_level_col])])
	robot_complete.extend([int(this_row[robot_complete_col])])
	robot_delivery.extend([int(this_row[robot_delivery_col])])
	work_load.extend([int(this_row[work_load_col])])
	human_complete.extend([int(this_row[human_complete_col])])
	time_step.extend([t])

	prioritize = 1 if this_row[prioritize_charge_col] == 'TRUE' else 0
	prioritize_charge.extend([prioritize])

	t+= 1


robot_state = np.array(robot_state)
robot_state.resize(robot_state.shape[0])


battery = np.array(battery)
battery.resize(battery.shape[0])


robot_complete = np.array(robot_complete)
robot_complete.resize(robot_complete.shape[0])


robot_delivery = np.array(robot_delivery)
robot_delivery.resize(robot_delivery.shape[0])


work_load = np.array(work_load)
work_load.resize(work_load.shape[0])


human_complete = np.array(human_complete)
human_complete.resize(human_complete.shape[0])


human_complete = np.array(human_complete)
human_complete.resize(human_complete.shape[0])



prioritize_charge = np.array(prioritize_charge)
prioritize_charge.resize(prioritize_charge.shape[0])

time_step = np.array(time_step)
time_step.resize(time_step.shape[0])

plt.figure(1)
plt.plot(time_step, battery/5.0, label = 'battery')
plt.plot(time_step, robot_state, label = 'robot_state')
plt.plot(time_step, prioritize_charge, label = "prioritize_charge")
plt.grid()
plt.legend()

plt.figure(2)
plt.plot(time_step, human_complete,  label = 'human_complete')
plt.plot(time_step, robot_complete, label = 'robot_complete')
plt.plot(time_step, robot_state, label = 'robot_state')
plt.grid()
plt.legend()

plt.figure(3)
plt.plot(time_step, work_load,  label = 'work_load')
plt.plot(time_step, robot_state, label = 'robot_state')
plt.grid()
plt.legend()

print battery/5.0

plt.show()

