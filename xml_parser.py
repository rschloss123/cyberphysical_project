import xml.etree.ElementTree as ET
import csv 

def xml_parser():


	tree = ET.parse('trace.xml')

	root = tree.getroot()

	elements = [] 


	for child in root:
		dict_local = {}

		for grandchild in child:
			for grand_grandchild in grandchild:
				# print grand_grandchild.tag
				name = grand_grandchild.attrib['variable']
				val = grand_grandchild.text

				dict_local[name] = val

			elements.append(dict_local)


	# list of dictionaries 
	return elements

def list_of_dicts_to_csv(elements):

	keys = elements[0].keys()

	with open('states_and_vals.csv', 'wb') as output_file:
		dict_writer = csv.DictWriter(output_file, keys)
		dict_writer.writeheader()
		dict_writer.writerows(elements)


def main():
	states_and_vals = xml_parser()
	list_of_dicts_to_csv(states_and_vals)

if __name__ == '__main__': 
	main()